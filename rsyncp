#! /usr/bin/env python

from __future__ import print_function

import argparse
import sys,os
import subprocess
import numpy
import threading
from Queue import Queue,PriorityQueue
import string

def fileSizeNameWalker(p):
    root = os.path.dirname(os.path.normpath(p))
    rooted = os.path.dirname(p)

    for path, dirs, files in os.walk(p):
        for f in files:
            f = os.path.normpath(os.path.join(path,f))
            if os.path.islink(f):
                fsize = 0
            else:
                fsize = os.stat(f).st_size
            yield (fsize,f.replace(root,rooted,1))

class FileListWorker(threading.Thread):
    def __init__(self,pathQ,resultQ,computeTotals=False):
        threading.Thread.__init__(self)
        self._pQ = pathQ
        self._sfQ = resultQ
        self._computeTotals = computeTotals

    def run(self):
        while True:
            p = self._pQ.get()
            if p is None:
                return

            if os.path.isabs(p):
                root = os.path.dirname(p)
            else:
                root = os.getcwd()
                if p[-1] == '/':
                    root = os.path.abspath(os.path.join(root,p))
            rooted = root+'/.'

            p = os.path.abspath(p)

            if os.path.islink(p):
                # it's a link - zero size
                self._sfQ.put((0,p.replace(root,rooted,1)))
                continue
            elif os.path.isfile(p):
                # it's a file - single size
                self._sfQ.put((os.stat(p).st_size,p.replace(root,rooted,1)))
                continue

            if self._computeTotals:
                # compute total directory size
                total  = 0
                for path, dirs, files in os.walk(p):
                    for f in files:
                        f = os.path.normpath(os.path.join(path,f))
                        if not os.path.islink(f):
                            total += os.stat(f).st_size
                self._sfQ.put((total,p.replace(root,rooted,1)))
                continue

            for fs,fn in fileSizeNameWalker(p.replace(root,rooted,1)):
                self._sfQ.put((fs,fn))

class FileListWriter(threading.Thread):
    def __init__(self,resultQ,outputPrefix,nChunks,nullSeparated=False):
        threading.Thread.__init__(self)
        self.sfQ = resultQ
        self.nChunks = nChunks
        if nullSeparated:
            self.sep = '\x00'
        else:
            self.sep = '\n'

        self.sizes = numpy.zeros(self.nChunks)
        self.outfiles = []
        if self.nChunks == 1:
            self.outfiles.append(open(outputPrefix,'w'))
        else:
            for i in range(self.nChunks):
                self.outfiles.append(open("%s_%02d"%(outputPrefix,i),'w'))

    def __del__(self):
        for i in range(self.nChunks):
            print(self.outfiles[i].name, self.sizes[i])
            self.outfiles[i].close

    def run(self):        
        while True:
            fs,fn = self.sfQ.get()
            if fn is None:
                break
            
            idx = numpy.argmin(self.sizes)
            self.outfiles[idx].write(fn+self.sep)
            self.sizes[idx] += fs
                
class FileListWriterSorted(FileListWriter):
    def run(self):
        fileList = []
        while True:
            data = self.sfQ.get()
            if data[1] is None:
                break
            fileList.append(data)
        # sort the file list
        fileList.sort(key=lambda tup: tup[0], reverse=True)
        
        for fs,fn in fileList:
            idx = numpy.argmin(self.sizes)
            self.outfiles[idx].write(fn+self.sep)
            self.sizes[idx] += fs
            
class FileListRsync(threading.Thread):
    def __init__(self,resultQ,dest,rsyncOpts):
        threading.Thread.__init__(self)
        self.sfQ = resultQ

        cmd =  ['rsync','-0','--files-from=-'] + rsyncOpts.split()+['/',dest]

        self.proc = subprocess.Popen(cmd,stdin=subprocess.PIPE)

    def run(self):
        while True:
            fs,fn = self.sfQ.get()
            if fn is None:
                break
            if os.path.isdir(fn):
                for fs,fn in fileSizeNameWalker(fn):
                    self.proc.stdin.write(fn+'\x00')
            else:
                self.proc.stdin.write(fn+'\x00')
        self.proc.stdin.close()
        self.proc.wait()

if __name__ == '__main__':

    MAX_FILE_LIST_WORKERS = 100
    DEFAULT_RSYNC_OPTS = "-qaASXx"
    DEFAULT_OUTFILE_PREFIX = os.path.join(os.environ["HOME"], "rsync_files." + str(os.getpid()))

    parser = argparse.ArgumentParser(description='Parallel rsync')
    parser.add_argument('-p','--parallel',default=10,type=int,metavar='N',help="split up transfer into N chunks")
    parser.add_argument('-r','--rsync-options',default=DEFAULT_RSYNC_OPTS,metavar='OPTS',help="options used for rsync (default: %s)"%DEFAULT_RSYNC_OPTS)
    parser.add_argument('-d','--destination',metavar='DST',help="run rsync program to rsync file to DST, otherwise write list of files")
    parser.add_argument('-o','--output-prefix',default=DEFAULT_OUTFILE_PREFIX,metavar='OUT',help="prefix for output file lists (default: %s)"%DEFAULT_OUTFILE_PREFIX)
    parser.add_argument('-t','--totals',default=False,action='store_true',help="use directory sizes rather than individual files")
    parser.add_argument('-n','--null-separated',default=False,action='store_true',help="produce NULL separated file list, rather than newline separated")
    parser.add_argument('-b','--balanced',default=False,action='store_true',help="keep all file sizes in memory to produce a balanced distribution")
    parser.add_argument('sources',nargs='+',metavar='SRC',help="the source(s) to be copied. If a source is a directory and ends with a / then copy only the contents of the directory.")
    args=parser.parse_args()

    # create queues
    pathQueue = Queue(1000)
    fileQueue = PriorityQueue(1000)

    # start the file list workers
    nFilelistWorkers = min(len(args.sources),MAX_FILE_LIST_WORKERS)
    filelistWorkers = []
    for i in range(nFilelistWorkers):
        fw = FileListWorker(pathQueue,fileQueue,computeTotals=args.totals)
        fw.start()
        filelistWorkers.append(fw)

    # start the output workers
    outputWorkers = []
    if args.destination is None:
        if args.balanced:
            OutWorker = FileListWriterSorted
        else:
            OutWorker = FileListWriter            
        ow = OutWorker(fileQueue,args.output_prefix,args.parallel,nullSeparated=args.null_separated)
        outputWorkers.append(ow)
    else:
        for i in range(args.parallel):
            ow = FileListRsync(fileQueue,args.destination,args.rsync_options)
            outputWorkers.append(ow)
    for ow in outputWorkers:
        ow.start()

    # feed the path queue
    for p in args.sources:
        pathQueue.put(p)

    # poison the file list workers and wait for them
    for i in range(nFilelistWorkers):
        pathQueue.put(None)
    for fw in filelistWorkers:
        fw.join()

    # poison the output workers and wait for them
    for ow in outputWorkers:
        fileQueue.put((None,None))
    for ow in outputWorkers:
        ow.join()
            
        
            

